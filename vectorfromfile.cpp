#include<iostream>
#include<string>
#include<vector>
#include<fstream>
#include<cstdio>
#include<string>
using namespace std;

void display(vector<vector<bool> >& board){
// this will display the new board fam. 
for(int i = 0; i < board.size(); i++) 
{ for (int j=0;j<board[i].size(); j++)
	if (board[i][j])
		cout << "O";
	else
		cout << ".";
	cout << endl;
}
}



void parseBoard(vector<vector<bool> >& board, vector<bool>& column){
/*i want to post whats in file to vector in T/F values
 should only be run once each new game*/
//first get the file open bro

FILE* f =fopen("testfile","rb");
char c;
while(fread(&c,1,1,f) != 0){
	switch (c) {
	case 'O':
		column.push_back(true);
		break;
	case '.':
		column.push_back(false);
		break;
	case '\n':
		board.push_back(column);
		column.clear();
		break;
	
}

}
fclose(f);
}

bool iscellalive(const vector<vector<bool> >& board, int i, int j){
//this function will return true if the next cell generation will be alive
int truths;  
//corners
 
if (i == 0 && j == 0)
	return false;
else if (i == 0 && j == board.size() -1)
	return false;
else if (i == board[i].size() -1 && j == 0)
	return false;
else if (i == board[i].size() -1 && j == board.size() -1)
	return false;
//sides
else if (i == 0 && j != 0 && j != board.size() -1) 
	return false;
else if (i != 0 && i != board[i].size() -1 && j == 0)
	return false;
else if (i !=0 && i != board[i].size() -1 && j == board.size() -1)
	return false;
else if (i == board[i].size() -1 && j != 0 && j != board.size() -1)
	return false;

else {
	for(int row= i-1; i<= 1 ; row++){
		for (int column= j-1; j<=-1; j++){
			if (board[j][i] == true)
				truths++;
				cout<< truths << endl;
}
}
} 
// done checking how many neighbors are alive now return true of false 
if(truths == 2|| truths == 3)
	return true;
else 
	return false;

}

void cellcycle(vector<vector<bool> >& oldboard, vector<vector<bool> >& newboard){
/*the 2 for loops basically go through 0,0 all the way to the end. we do this
 so that we can run the life check for all cells in one function*/
int i,j,count ;
count = 0;

for( i = 0; i < oldboard.size(); i++) 
{ for ( j=0;j<oldboard[i].size(); j++)
		newboard[i][j] =  iscellalive(oldboard,i,j);
}
}


void vectortransfer(vector<vector<bool> >& oldboard, vector<vector<bool> >& newboard){
//start all over again and tranfer the new to the old
for (int i = 0; i<oldboard.size(); i++){
	for(int j=0;j<oldboard[i].size();j++){
		oldboard[i][j] = newboard[i][j];
	
}
}
}

int main(){
// create two vecters old and new 
vector<vector<bool> > oldboard;
vector<vector<bool> > newboard;
/*now create a vector that will push the columns into the vector, can be 
used twice */
vector<bool> column;
parseBoard(oldboard,column);
parseBoard(newboard,column);
display(oldboard);
cout << endl;
cellcycle(oldboard,newboard);
display(newboard);

}
